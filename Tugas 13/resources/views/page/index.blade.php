@extends('layout.master')
@section('judul')
    Sanberbook
@endsection    

@section('subtitle')
    Social Media Developer Santai Berkualitas
@endsection

@section('content')    
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>

    <h2>Benefit Join di SanberBook</h2>

    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>

    <h3>Cara Bergabung ke SanberBook</h3>

    <ol>
        <li>Mengunjuki Website ini</li>
        <li>Mendaftar di <a href="/Register">Form Sign up</a></li>
        <li>Selesai!</li>
    </ol>
@endsection