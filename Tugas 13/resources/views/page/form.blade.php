@extends('layout.master')
@section('judul')
    Buat Account Baru!
@endsection    

@section('subtitle')
    Sign Up Form
@endsection

@section('content')    
    <form action="/Welcome" method="POST">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="first"><br><br>
        <label>Last name:</label><br>
        <input type="text" name="last"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="jk" value="Male">Male<br>
        <input type="radio" name="jk" value="Female">Female<br><br>
        <label>Nationality:</label><br>
        <select name="negara">
            <option value="indonesia">Indonesia</option>
            <option value="english">English</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapura">Singapura</option>
        </select><br><br>
        <label>Languange Spoken:</label><br>
        <input type="checkbox" name="languange" value="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="languange" value="english">English<br>
        <input type="checkbox" name="languange" value="other">Other<br><br>

        <label>Bio:</label><br>
        <textarea name="bio"></textarea><br><br>
        <input type="submit" value="Sign up">
    </form>
@endsection
