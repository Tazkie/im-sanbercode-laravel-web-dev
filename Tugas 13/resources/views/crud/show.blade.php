@extends('layout.master')
@section('judul')
    About Cast
@endsection    

@section('subtitle')
    Data Cast
@endsection

@section('content')
<h1 class="text-danger">{{$cast->nama}}</h1>
<h4 class="text-dark">{{$cast->umur}} Tahun</h4>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-primary">Kembali</a>
@endsection