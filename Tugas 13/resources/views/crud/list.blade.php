@extends('layout.master')
@section('judul')
    List Cast
@endsection    

@section('subtitle')
    Data Cast
@endsection

@section('content')
    
<a href="/cast/create" class="btn btn-primary btn-sm mb-3">Add Cast</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td>
                    <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                    <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1 btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection