<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('page.form');
    }

    public function welcome(Request $request){
        $pertama = $request['first'];
        $kedua = $request['last'];
        return view('page.welcome' , ['first' => $pertama, 'last' => $kedua]);
    }

    public function table(){
        return view('partial.datatables');
    }

    public function data(){
        return view('partial.table');
    }
}
