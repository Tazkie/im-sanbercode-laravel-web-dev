<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function welcome(Request $request){
        $pertama = $request['first'];
        $kedua = $request['last'];
        return view('welcome' , ['first' => $pertama, 'last' => $kedua]);
    }
}
